export default interface Products {
  id: number;
  name: string;
  price: number;
}
