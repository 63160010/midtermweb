import { ref } from "vue";
import { defineStore } from "pinia";
import type Products from "@/types/Products";

export const ProDucts = defineStore("product", () => {
  const product = ref<Products[]>([]);
  const sum = ref(0);
  function add(id: number, name: string, price: number) {
    product.value.push({ id, name, price });
    sum.value += price;
  }
  return { product, add, sum };
});
